<html>
    <head>
    {{ HTML::style('css/bootstrap.min.css') }}
    <!-- Optional theme -->
    {{ HTML::style('css/bootstrap-theme.min.css') }}

    <!-- Latest compiled and minified JavaScript -->
    {{ HTML::script('js/bootstrap.min.js') }}
    </head>
    <body>
        <div>
            @if(Auth::check())
                {{ HTML::link('password/change', 'Change Password', array('id' => 'change_password'))}}
                -
                {{ HTML::link('link', 'Logout', array('id' => 'logout'))}}
            @endif
        </div>
        <div>header</div>
        <div>@yield('content')</div>
        <div>footer</div>
    </body>
</html>