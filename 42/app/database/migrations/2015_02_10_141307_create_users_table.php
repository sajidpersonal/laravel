<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('users', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('username', 500);
                $table->string('email', 500);
                $table->string('password', 500);
                $table->timestamps();
                $table->string('_token', 200);
                $table->string('remember_token', 200);
            });
            Schema::create('password_reminders', function($table)
            {
                $table->string('email', 500);
                $table->string('token', 500);
                $table->timestamp('created_at');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('users');
            Schema::drop('password_reminders');
	}

}
