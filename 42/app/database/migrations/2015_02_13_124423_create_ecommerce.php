<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerce extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('customers', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('fname', 500);
                $table->string('lname', 500);
                $table->string('class', 500);
                $table->string('room', 500);
                $table->string('building', 500);
                $table->string('address1', 500);
                $table->string('address2', 500);
                $table->string('city', 500);
                $table->string('state', 500);
                $table->string('postal_code', 500);
                $table->string('country', 500);
                $table->string('phone', 500);
                $table->string('email', 500);
                $table->string('voice_mail', 500);
                $table->string('billing_address', 500);
                $table->string('billing_city', 500);
                $table->string('billing_region', 500);
                $table->string('billing_postal_code', 500);
                $table->string('billing_country', 500);
                $table->string('ship_address', 500);
                $table->string('ship_city', 500);
                $table->string('ship_region', 500);
                $table->string('ship_postalcode', 500);
                $table->string('ship_country', 500);
                $table->timestamp('date_entered');
            });
            Schema::create('categories', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('cname', 500);
                $table->text('description');
                $table->string('picture', 500);
                $table->tinyInteger('active');
            });
            Schema::create('products', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('sku', 500);
                $table->text('idsku');
                $table->string('vendor_product_id', 500);
                $table->text('pdescription');
                $table->string('pname', 500);
                $table->bigInteger('category_id');
                $table->float('quantity_per_unit');
                $table->float('unit_price');
                $table->string('msrp', 500);
                $table->string('available_size', 500);
                $table->string('available_colors', 500);
                $table->string('size', 500);
                $table->string('color', 500);
                $table->float('discount');
                $table->float('unit_weight');
                $table->float('unit_instock');
                $table->float('units_onorder');
                $table->string('reorder_level', 500);
                $table->float('product_available');
                $table->float('discount_available');
                $table->string('picture', 500);
                $table->string('note');
            });
            Schema::create('mail', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('mail_address', 500);
                
            });
            Schema::create('theme', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('tname', 500);
                $table->tinyInteger('active');
                
            });
            Schema::create('error_logs', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('error', 500);
                $table->timestamp('created_at');
                
            });
	}
        

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('customers');
            Schema::drop('categories');
            Schema::drop('products');
            Schema::drop('mail');
            Schema::drop('theme');
            Schema::drop('error_lofgs');
	}

}
